#pragma once

#include <string>
#include <vector>

typedef unsigned int u32;

enum Channel : int
{
	RED = 0,
	GREEN = 1,
	BLUE = 2,
	NUM = 3
};

class ppm
{
	typedef unsigned char pixel;

	void Init();

	ppm(const ppm& other) = delete;
	const ppm& operator=(const ppm& other) = delete;

public:

	ppm();
	ppm(std::string &fname);
	ppm(const unsigned int width, const unsigned int height);

	~ppm();

	bool Read(const std::string& fname);
	void Write(const std::string& fname);

	pixel* color[Channel::NUM];

	u32 mHeight;
	u32 mWidth;
	u32 mColorValue;

	u32 mSize;

};

