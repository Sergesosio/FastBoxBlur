#include "ppm.h"

#include <iostream>
#include <fstream>
#include <sstream>

ppm::ppm()
{
	Init();
}

ppm::ppm(std::string &fname)
{
	Init();
	Read(fname);
}

ppm::ppm(const unsigned int _width, const unsigned int _height)
{
	mWidth = _width;
	mHeight = _height;

	mSize = mWidth * mHeight;

	for (size_t channel = 0; channel < Channel::NUM; channel++)
		color[channel] = new pixel[mSize];

}

void ppm::Init() 
{
	for (size_t channel = 0; channel < Channel::NUM; channel++)
		color[channel] = nullptr;
	
	mWidth = 0;
	mHeight = 0;
	mColorValue = 255;
}

ppm::~ppm()
{
	for (size_t channel = 0; channel < Channel::NUM; channel++)
		if (color[channel] != nullptr)
			delete[] color[channel];
}

bool ppm::Read(const std::string& fname)
{
	std::ifstream file(fname.c_str(), std::ios::in | std::ifstream::binary);

	if (file.is_open())
	{
		std::string line;
		std::stringstream ss(line);

		std::getline(file, line);

		if (line != "P6")
		{
			std::cout << "Error. Unrecognized file format." << std::endl;
			return false;
		}

		std::getline(file, line);
		ss << line;
		ss >> mWidth;
		ss.clear();

		std::getline(file, line);
		ss << line;
		ss >> mHeight;
		ss.clear();

		std::getline(file, line);
		ss << line;
		ss >> mColorValue;

		mSize = mWidth * mHeight;

		for (size_t channel = 0; channel < Channel::NUM; channel++)
			color[channel] = new pixel[mSize];

		char pixel;
		for (size_t i = 0; i < mSize; i++)
		{
			for (size_t channel = 0; channel < Channel::NUM; channel++)
			{
				file.read(&pixel, 1);
				color[channel][i] = pixel;
			}
		}
		file.close();
	} 
	else
	{
		std::cout << "unable to open " << fname << std::endl;
		return false;
	}

	return true;
}

void ppm::Write(const std::string& fname)
{
	std::ofstream file(fname.c_str(), std::ios::out | std::ios::binary);
	if (file.is_open())
	{
		file << "P6\n";
		file << mWidth;
		file << " ";
		file << mHeight << "\n";
		file << mColorValue << "\n";

		char pixel;
		for (size_t i = 0; i < mSize; i++)
		{
			for (size_t channel = 0; channel < Channel::NUM; channel++)
			{
				pixel = color[channel][i];
				file.write(&pixel, 1);
			}
		}
		file.close();
	}
}