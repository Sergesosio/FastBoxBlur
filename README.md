# FastBoxBlur

Quick implementation of the "even better box blur" alglorithm as described [here](http://amritamaz.net/blog/understanding-box-blur/).
The alglorithm seems to scale very well with higher blur radius.
It's done both single- and multithreaded. Multithreading doesn't seem to be faster for small and medium sized images, and might even be slower. It does however have a huge impact on large images.
You might get better multithreaded performance if you do each channel on a different thread instead of dividing the image matrix in parts.

* Requires C++11 to build. 

#### Source image

![source image](https://raw.githubusercontent.com/Sergesosio/FastBoxBlur/master/file.jpg)

#### Blurred image 
Radius 2 and 3 passes, in ~0.037 seconds.

![blurred image](https://raw.githubusercontent.com/Sergesosio/FastBoxBlur/master/file_single.jpg)
