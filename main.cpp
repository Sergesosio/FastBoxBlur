#include <iostream>
#include <thread>
#include <vector>
#include <functional>

#include <windows.h>

#include "ppm.h"

class Timer 
{
private:
	static LARGE_INTEGER frequency;
	static LARGE_INTEGER t1, t2;
	static double elapsedTime;
public:
	static void Start() 
	{
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&t1);
	}

	static void Stop()
	{
		QueryPerformanceCounter(&t2); 
		elapsedTime = (float)(t2.QuadPart - t1.QuadPart) / frequency.QuadPart;
		std::cout << elapsedTime << " sec" << std::endl;
	}
};

LARGE_INTEGER Timer::frequency;
LARGE_INTEGER Timer::t1, Timer::t2;
double Timer::elapsedTime;

typedef std::function<void(u32, u32)> WorkLoop;

void DoWork(WorkLoop function, u32 arrayLength, u32 numThreads)
{
	std::vector<std::thread> threads;
	std::vector<int> bounds;

	int delta = arrayLength / numThreads;
	int reminder = arrayLength % numThreads;
	int N1 = 0, N2 = 0;
	bounds.push_back(N1);

	for (size_t i = 0; i < numThreads; ++i)
	{
		N2 = N1 + delta;
		if (i == numThreads - 1)
			N2 += reminder;
		bounds.push_back(N2);
		N1 = N2;
	}

	for (size_t i = 0; i < numThreads - 1; i++)
	{
		threads.push_back(std::thread(function, bounds[i], bounds[i + 1]));
	}

	function(bounds[numThreads - 1], bounds[numThreads]);

	for (size_t i = 0; i < numThreads - 1; i++)
	{
		threads[i].join();
	}
}

void BoxBlur(ppm& source, ppm& target, u32 radius, u32 passes)
{
	if (radius == 0 || passes == 0 || passes > 3)
		return;

	u32 width = source.mWidth;
	u32 height = source.mHeight;

	if (radius > (width / 2) || radius > (height / 2))
		return;

	u32 radiusPlus = radius + 1;
	u32 diameter = radius * 2 + 1;
	u32 xbound = width - radius;
	u32 ybound = height - radius;
	u32 sum;

	ppm temp(width, height);

	ppm* src = &source;

	for (size_t pass = 0; pass < passes; pass++)
	{
		for (size_t channel = 0; channel < Channel::NUM; channel++)
		{
			// Horizontal blur
			for (size_t y = 0; y < height; y++)
			{
				u32 offset = width * y;
				sum = src->color[channel][offset];

				//Calculate first pixel
				for (size_t i = 1; i < radiusPlus; i++)
					sum += src->color[channel][offset + i];

				temp.color[channel][offset] = sum / radiusPlus;

				u32 x = 1;
				//Left edge case
				while (x < radiusPlus)
				{
					sum += src->color[channel][offset + (x + radius)];
					temp.color[channel][offset + x] = sum / (radiusPlus + x);
					++x;
				}

				//center case
				while (x < xbound)
				{
					sum -= src->color[channel][offset + (x - radiusPlus)];
					sum += src->color[channel][offset + x + radius];
					temp.color[channel][offset + x] = sum / diameter;
					++x;
				}

				//right edge case
				u32 counter = 1;
				while (x < width)
				{

					sum -= src->color[channel][offset + (x - radiusPlus)];
					temp.color[channel][offset + x] = sum / (diameter - counter);
					++counter;
					++x;
				}
			}

			// Vertical blur
			for (size_t x = 0; x < width; x++)
			{
				sum = temp.color[channel][x];

				// Calculate first pixel
				for (size_t i = 1; i < radiusPlus; i++)
					sum += temp.color[channel][width * i + x];

				target.color[channel][x] = sum / radiusPlus;

				u32 y = 1;
				// Top edge case 
				while (y < radiusPlus)
				{
					sum += temp.color[channel][width * (y + radius) + x];
					target.color[channel][width * y + x] = sum / (radiusPlus + y);
					++y;
				}

				// center case
				while (y < ybound)
				{
					sum -= temp.color[channel][width * (y - radiusPlus) + x];
					sum += temp.color[channel][width * (y + radius) + x];
					target.color[channel][width * y + x] = sum / diameter;
					++y;
				}

				// Bottom edge case
				u32 counter = 1;
				while (y < height)
				{
					sum -= temp.color[channel][width * (y - radiusPlus) + x];
					target.color[channel][width * y + x] = sum / (diameter - counter);
					counter++;
					y++;
				}

			}
		}

		src = &target;
	}
}

void BoxBlurThreaded(ppm& source, ppm& target, u32 radius, u32 passes)
{
	if (radius == 0 || passes == 0 || passes > 3)
		return;

	u32 width = source.mWidth;
	u32 height = source.mHeight;

	if (radius > (width / 2) || radius > (height / 2))
		return;

	u32 radiusPlus = radius + 1;
	u32 diameter = radius * 2 + 1;
	u32 xbound = width - radius;
	u32 ybound = height - radius;
	
	ppm temp(width, height);

	ppm* src = &source; 

	for (size_t i = 0; i < passes; i++)
	{
		for (size_t channel = 0; channel < Channel::NUM; channel++)
		{
			WorkLoop horizontalBlur = [=, &temp](u32 from, u32 to) -> void {

				// Horizontal blur
				for (size_t y = from; y < to; y++)
				{
					u32 offset = width * y;
					u32 sum = src->color[channel][offset];

					//Calculate first pixel
					for (size_t i = 1; i < radiusPlus; i++)
						sum += src->color[channel][offset + i];

					temp.color[channel][offset] = sum / radiusPlus;

					u32 x = 1;
					//Left edge case
					while (x < radiusPlus)
					{
						sum += src->color[channel][offset + (x + radius)];
						temp.color[channel][offset + x] = sum / (radiusPlus + x);
						++x;
					}

					//center case
					while (x < xbound)
					{
						sum -= src->color[channel][offset + (x - radiusPlus)];
						sum += src->color[channel][offset + x + radius];
						temp.color[channel][offset + x] = sum / diameter;
						++x;
					}

					//right edge case
					u32 counter = 1;
					while (x < width)
					{

						sum -= src->color[channel][offset + (x - radiusPlus)];
						temp.color[channel][offset + x] = sum / (diameter - counter);
						++counter;
						++x;
					}
				}

			};

			DoWork(horizontalBlur, height, 4);

			WorkLoop verticalBlur = [=, &temp, &target](u32 from, u32 to) {
				// Vertical blur
				for (size_t x = from; x < to; x++)
				{
					u32 sum = temp.color[channel][x];

					// Calculate first pixel
					for (size_t i = 1; i < radiusPlus; i++)
						sum += temp.color[channel][width * i + x];

					target.color[channel][x] = sum / radiusPlus;

					u32 y = 1;
					// Top edge case 
					while (y < radiusPlus)
					{
						sum += temp.color[channel][width * (y + radius) + x];
						target.color[channel][width * y + x] = sum / (radiusPlus + y);
						++y;
					}

					// center case
					while (y < ybound)
					{
						sum -= temp.color[channel][width * (y - radiusPlus) + x];
						sum += temp.color[channel][width * (y + radius) + x];
						target.color[channel][width * y + x] = sum / diameter;
						++y;
					}

					// Bottom edge case
					u32 counter = 1;
					while (y < height)
					{
						sum -= temp.color[channel][width * (y - radiusPlus) + x];
						target.color[channel][width * y + x] = sum / (diameter - counter);
						counter++;
						y++;
					}

				}
			};

			DoWork(verticalBlur, width, 4);

		}

		src = &target;
	}
}

int main(int argc, char* argv[])
{
	std::string fname = std::string("file.ppm");

	std::cout << "Loading image..." << std::endl;

	ppm image;
	if (image.Read(fname) == false)
		return 0;

	ppm image_single(image.mWidth, image.mHeight);
	ppm image_multi(image.mWidth, image.mHeight);

	std::cout << "Blurring multithreaded" << std::endl;
	Timer::Start();
	BoxBlurThreaded(image, image_multi, 20, 3);
	Timer::Stop();

	std::cout << "Blurring singlethreaded" << std::endl;
	Timer::Start();
	BoxBlur(image, image_single, 2, 3);
	Timer::Stop();

	std::cout << "writing to disk." << std::endl;
	image_multi.Write("file_multi.ppm");
	image_single.Write("file_single.ppm");

	std::cout << "Done! Press ENTER to exit." << std::endl;

	std::cin.get();
	return 0;
}

